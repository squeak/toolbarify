var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/dom");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var methods = {

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: set layout and to refresh it on resize
    ARGUMENTS: ( ø )
    RETURN: void
  */
  initialize: function () {

    var toolbar = this;

    // GET LAYOUT POSISION
    toolbar.isShown = true;
    toolbar.layoutPosition = toolbar.layout.getPosition(true);
    toolbar.sizeToRemove = toolbar.layout.getSizeToRemove();
    toolbar.elResizeCss = {
      height: getCssValue(toolbar.$el, "height"),
      width: getCssValue(toolbar.$el, "width"),
      // height: toolbar.$el[0].style.height,
      // width: toolbar.$el[0].style.width,
    };

    function getCssValue ($el, cssKey) {
      return $el.clone().appendTo('body').wrap('<div style="display: none"></div>').css(cssKey);
    };

    // REFRESH LAYOUT
    toolbar.layout.refresh();

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REFRESH
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  refresh: function () {
    var toolbar = this;
    toolbar.layout.elementRefresh();
    toolbar.layout.toolbarRefresh();
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  UTILITIES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get's the bar size in it's orientation, and 0 in the other
    ARGUMENTS: ( ø )
    RETURN: { horizontal: <number>, vertical: <number>, } « one will be the bar size, the other zero »
  */
  thisBarSizeRemove: function () {
    var now = { horizontal: 0, vertical: 0, }
    now[this.orientation] = this.size;
    return now;
  },

  /**
    DESCRIPTION: get bar position offset
    ARGUMENTS: ( ?removeCurrentBarSize <boolean> )
    RETURN: { top: <number>, bottom: <number>, left: <number>, right: <number>, }
  */
  getPosition: function (removeCurrentBarSize) {
    var toolbar = this;
    var layoutPosition = { top: 0, bottom: 0, left: 0, right: 0, };
    _.each(toolbar.toolbars, function (tb) {
      layoutPosition[tb.position] += tb.size;
    });
    if (removeCurrentBarSize) layoutPosition[toolbar.position] -= toolbar.size;
    return layoutPosition;
  },

  /**
    DESCRIPTION: get cumulated size of bars for horizontal and vertical orientations
    ARGUMENTS: ( ø )
    RETURN: { horizontal: <number>, vertical: <number>, }
  */
  getSizeToRemove: function () {
    var toolbar = this;
    var toRemove = { horizontal: 0, vertical: 0, };
    _.each(toolbar.toolbars, function (tb) {
      toRemove[tb.orientation] += tb.size;
    });
    toRemove[toolbar.orientation] -= toolbar.size;
    return toRemove;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REFRESH LAYOUTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: refresh toolbar dimension and position
    ARGUMENTS: ( ø )
    RETURN: void
  */
  toolbarRefresh: function () {

    var toolbar = this;
    var thisBarSizeRemove = toolbar.layout.thisBarSizeRemove();

    if (toolbar.orientation == "vertical") toolbar.$toolbar.css({
      top: +toolbar.$el.css("top").replace(/px$/, "") + toolbar.layoutPosition.top,
      height: toolbar.$el.height() - thisBarSizeRemove.horizontal,
      width: toolbar.size,
      // height: window.innerHeight,
    })

    else toolbar.$toolbar.css({
      left: +toolbar.$el.css("left").replace(/px$/, "") + toolbar.layoutPosition.left,
      height: toolbar.size,
      width: toolbar.$el.width() - thisBarSizeRemove.vertical,
    });

    toolbar.$toolbar.css(toolbar.position, 0 + toolbar.layoutPosition[toolbar.position]);

  },

  /**
    DESCRIPTION: refresh element dimension and position
    ARGUMENTS: ( ø )
    RETURN: void
  */
  elementRefresh: function () {

    var toolbar = this;

    toolbar.$toolbar.removeClass("horizontal vertical");
    toolbar.$toolbar.addClass(toolbar.orientation);

    toolbar.$el.css({
      marginLeft: "",
      marginTop: "",
      height: "",
      width: "",
    });

    // IF THE ELEMENT HAD CSS STYLING THAT SHOULD BE REAPPLIED ON RESIZE BEFORE ADAPTING TO TOOLBAR PRESENCE
    if (toolbar.resetToStylesheetStylesOnResize) toolbar.$el.css({ width: "", height: "", });

    var toRemove = toolbar.layout.getPosition();
    var thisBarSizeRemove = toolbar.layout.thisBarSizeRemove();

    // TODO SO WHAT ABOUT IF THERE IS A MARGIN CURRENTLY SET ON EL ??
    // var currentElLeftMargin = +toolbar.$el.css("margin-left").replace(/px$/, "");
    // var currentElTopMargin = +toolbar.$el.css("margin-top").replace(/px$/, "");
    toolbar.$el.css({
      // "margin-left": currentElLeftMargin + toRemove.vertical,
      // "margin-top": currentElTopMargin + toRemove.horizontal,
      "margin-left": toRemove.left,
      "margin-top": toRemove.top,
      height: toolbar.$el.height() - thisBarSizeRemove.horizontal - toolbar.sizeToRemove.horizontal,
      width: toolbar.$el.width() - thisBarSizeRemove.vertical - toolbar.sizeToRemove.vertical,
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (toolbar) {
  var toolbarify = this;

  // ADD TOOLBAR TO LIST OF ALL TOOLBARS ATTACHED TO THIS ELEMENT
  var toolbarsElIndex = toolbarify.toolbarsElGetIndex(toolbar);
  if (toolbarsElIndex == -1) {
    toolbarify.toolbarsGroupedByElement.push({ $el: toolbar.$el, toolbars: [toolbar], });
    toolbar.elId = toolbarify.toolbarsGroupedByElement.length - 1;
  }
  else {
    toolbarify.toolbarsGroupedByElement[toolbarsElIndex].toolbars.push(toolbar);
    toolbar.elId = toolbarsElIndex;
  };
  toolbar.toolbars = toolbarify.toolbarsGroupedByElement[toolbar.elId].toolbars;

  //
  //                              LAYOUT

  // ATTACH LAYOUT METHODS
  toolbar.layout = {};
  _.each(methods, function (methodFunc, methodName) {
    toolbar.layout[methodName] = _.bind(methodFunc, toolbar);
  });

  // MAKE LAYOUT
  toolbar.layout.initialize();

  // MAKE CUSTOM DESTROY METHOD THAT REMOVE LAYOUT METHODS FROM toolbar OBJECT
  toolbar.destroy = function () {
    toolbar.$el.css({
      marginLeft: "",
      marginTop: "",
      height: "",
      width: "",
    });
    toolbar.$toolbar.remove();
    delete toolbar.layout;
    toolbar.toolbars = toolbarify.toolbarsGroupedByElement[toolbar.elId].toolbars = _.reject(toolbar.toolbars, function (too) { return too.id == toolbar.id });
  };

};
