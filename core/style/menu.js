var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
var Hammer = require("hammerjs");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  BUTTON CORRESPONDANCES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var buttonCorrespondances = {
  right: "bottom",
  left: "bottom",
  top: "right",
  bottom: "left",
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (toolbar) {
  var toolbarify = this;

  // MAKE TOOLBAR FAB
  if (toolbar.displayToolbarFab) {
    var fabOptions = {
      $container: toolbar.$el,
      position: {},
      class: "toolbarify-toggle_button",
      click: toolbar.toggle,
      icomoon: "ellipsis-h",
    };
    // fabOptions.position[toolbar.position] = "stick";
    fabOptions.position[toolbar.position] = "stick";
    fabOptions.position[buttonCorrespondances[toolbar.position]] = "stick";

    toolbar.toggleButton = uify.fab(fabOptions);
  };

  // hide toolbar, and hide it again every time it's clicked
  toolbar.isShown = false;
  toolbar.$toolbar.css(toolbar.menuCorrespondance[toolbar.position], "100%");
  // enable animation, but wait half a second, so that it doesn't animate on hidding
  toolbar.setupTransition();
  toolbar.$toolbar.click(function () { toolbar.hide(); });

  toolbar.destroy = function () {
    if (toolbar.toggleButton) {
      toolbar.toggleButton.$button.remove();
      delete toolbar.toggleButton;
    };
    toolbar.$toolbar.remove();
  };

  // CREATE SWIPABLE AREA
  $swipableArea = toolbar.$el.div({ class: "toolbarify-swipable_area "+ toolbar.position, });

  // SWIPE SWIPABLE AREA
  var hammerSwipableArea = new Hammer($swipableArea[0]);
  hammerSwipableArea.on("swipe"+ toolbar.menuCorrespondance[toolbar.position], function (e) {
    toolbar.show();
  });

  // SWIPE SWIPABLE TOOLBAR WHEN IT'S SHOWN
  var hammerToolbar = new Hammer(toolbar.$toolbar[0]);
  hammerToolbar.on("swipe"+ toolbar.position, function (e) {
    toolbar.hide();
  });

};
