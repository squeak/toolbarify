var _ = require("underscore");
var $$ = require("squeak");
var addMethods = require("./add");
var makeToolbar = {
  condensed: require("./style/condensed"),
  hidden: require("./style/hidden"),
  menu: require("./style/menu"),
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (toolbar) {
  var toolbarify = this;

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ATTACH CREATION METHODS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  toolbar.add = {};
  _.each(addMethods, function (methodFunc, methodName) {
    toolbar.add[methodName] = _.bind(methodFunc, toolbar);
  });

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ADD GET BUTTON METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  toolbar.get = function (buttonName) { return _.findWhere(toolbar.list, { name: buttonName }); };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  TOGGLE TOOLBAR DISPLAY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  toolbar.menuCorrespondance = {
    left: "right",
    right: "left",
    top: "bottom",
    bottom: "top",
  };

  toolbar.toggle = function (noTransition) {
    if (toolbar.isShown) toolbar.hide(noTransition)
    else toolbar.show(noTransition);
  };

  toolbar.show = function (noTransition) {
    toolbar.isShown = true;
    if (toolbar.effectiveStyle == "menu") {
      if (noTransition) toolbar.diableTransition();
      toolbar.$toolbar.css(toolbar.menuCorrespondance[toolbar.position], 0);
      toolbar.setupTransition();
    }
    else toolbar.$toolbar.show();
  };

  toolbar.hide = function (noTransition) {
    toolbar.isShown = false;
    if (toolbar.effectiveStyle == "menu") {
      if (noTransition) toolbar.diableTransition();
      toolbar.$toolbar.css(toolbar.menuCorrespondance[toolbar.position], "100%");
      toolbar.setupTransition();
    }
    else toolbar.$toolbar.hide();
  };

  toolbar.setupTransition = function () {
    setTimeout(function () { toolbar.$toolbar.css("transition", toolbar.menuCorrespondance[toolbar.position] +" 0.5s"); }, 500);
  };
  toolbar.diableTransition = function () {
    toolbar.$toolbar.css("transition", "none");
  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE A CUSTOM TOGGLE BUTTON
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  toolbar.customToggleButton = function (buttonOptions) {

    if (toolbar.toggleButton) {
      toolbar.toggleButton.$button.remove();
      delete toolbar.toggleButton;
    };

    buttonOptions.click = toolbar.toggle;

    return buttonOptions;

  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FIGURE OUT EFFECTIVE STYLE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  toolbar.getEffectiveStyle = function (previousStyle) {
    return toolbar.smallScreenThreshold() ? toolbar.style_smallScreens : toolbar.style;
  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE TOOLBAR
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  toolbar.make = function () {

    //
    //                              CREATE DOM

    // CREATE TOOLBAR DIV
    toolbar.$toolbar = $('<div class="toolbarify"></div>');
    toolbar.$el[toolbar.where](toolbar.$toolbar);
    if (toolbar.color) toolbar.$toolbar.css("background-color", toolbar.color);
    if (toolbar.class) toolbar.$toolbar.addClass(toolbar.class);

    // CREATE CONTAINERS
    toolbar.$start = toolbar.$toolbar.div({ class: "start-buttons", });
    toolbar.$toolbar.div({ class: "intercalaire", });
    toolbar.$middle = toolbar.$toolbar.div({ class: "middle-buttons", });
    toolbar.$toolbar.div({ class: "intercalaire", });
    toolbar.$end = toolbar.$toolbar.div({ class: "end-buttons", });

    //
    //                              SETUP TOOLBAR STYLE

    toolbar.effectiveStyle = toolbar.getEffectiveStyle();
    toolbar.$toolbar.addClass("toolbarify-"+ toolbar.effectiveStyle);

    //
    //                              CREATE THE TYPE OF TOOLBAR FOR THIS STYLE

    if (makeToolbar[toolbar.effectiveStyle]) makeToolbar[toolbar.effectiveStyle].call(toolbarify, toolbar)
    else makeToolbar.condensed.call(toolbarify, toolbar);

    //                              ¬
    //

  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
