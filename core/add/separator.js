var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create a vertical or horizontal separator
  ARGUMENTS: (options <toolbarify·buttonOptions> « see README page »)
  RETURN: <toolbarifyButtonReturn>
*/
module.exports = function (options) {
  var toolbar = this;

  // set icomoon and class options
  if (!options) options = {};
  options.icomoon = toolbar.orientation == "vertical" ? "ellipsis-h" : "ellipsis-v"; // minus // circle-thin, square-o, circle2, square3, long-arrow-down
  options.class = (options.class ? options.class +" " : "") + "toolbarify-separator toolbarify-separator-"+ toolbar.orientation;

  // create and return button
  return toolbar.add.button(options);

};
