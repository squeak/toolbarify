var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create a vertical or horizontal separator
  ARGUMENTS: (options <toolbarify·buttonOptions> « see README page »)
  RETURN: <toolbarifyButtonReturn>
*/
module.exports = function (options) {
  var toolbar = this;

  // set icomoon and class options
  if (!options) options = {};
  if (!options.icomoon) options.icomoon = "spinner9";
  options.class = (options.class ? options.class +" " : "") + "toolbarify-spinner";

  // create and return button
  return toolbar.add.button(options);

};
