var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create a vertical or horizontal separator
  ARGUMENTS: (options <toolbarify·buttonOptions> « see README page »)
  RETURN: <toolbarifyButtonReturn>
*/
module.exports = function (options) {
  var toolbar = this;

  // set class option
  if (!options) options = {};
  options.class = (options.class ? options.class +" " : "") + "toolbarify-blank";

  // create and return button
  return toolbar.add.button(options);

};
