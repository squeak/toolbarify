var _ = require("underscore");
var $$ = require("squeak");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: create a vertical or horizontal separator
  ARGUMENTS: (options <toolbarify·buttonOptions> « see README page »)
  RETURN: <{
    <toolbarifyButtonReturn> with the additional methods:
    progressTick: <function( progressPercentage <number> « state of the progress, between 0 and 100 » )> « run this method to trigger progress change »,
    progressFinish: <function( keepButton <boolean> « pass true if progress button should not be » )> « run this method when finished »,
  }>
*/
module.exports = function (options) {
  var toolbar = this;

  // SET CLASS OPTION
  if (!options) options = {};
  options.class = (options.class ? options.class +" " : "") + "toolbarify-progress_bar";
  options.icomoon = " ";

  // CREATE BUTTON
  var progressButton = toolbar.add.button(options);

  // CHANGE PROGRESS LEVEL
  progressButton.progressTick = function (progressPercentage) {
    progressButton.$icon.css("height", progressPercentage +"%");
  };

  // FINISH
  progressButton.progressFinish = function (keepButton) {
    if (!keepButton) progressButton.remove()
    else {
      progressButton.$icon.addClass("icon-check");
      progressButton.$icon.css("height", 100 +"%");
    };
  };

  // RETURN BUTTON
  return progressButton;

};
