var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var uify = require("uify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: add button to toolbar
  ARGUMENTS: ( options <toolbarify·buttonOptions> « see README page » )
  RETURN: <toolbarify·buttonReturn> « see README page »
*/
module.exports = $$.scopeFunction({
  main: function (options) {
    var toolbar = this;

    //
    //                              ASKED OTHER SPECIFIC TYPE

    if (_.indexOf(["separator", "blank", "progressBar", "spinner"], options.type) !== -1) {
      var toolbarButtonType = options.type;
      delete options.type;
      return toolbar.add[toolbarButtonType](options);
    };

    //
    //                              CREATE BUTTON

    if (options.position == "start" || options.position == "top" || options.position == "left") options.$container = toolbar.$start
    else if (options.position == "middle") options.$container = toolbar.$middle
    else options.$container = toolbar.$end;

    // inline title if menu
    if (toolbar.effectiveStyle == "menu") options.inlineTitle = "right"
    else options.inlineTitle = undefined;

    // default css options
    if (toolbar.effectiveStyle == "condensed") {
      var defaultCss = {
        height: toolbar.size,
        width: toolbar.size,
        fontSize: toolbar.size - (options.padding*2 || 10),
        padding: 5,
      };
      options.css = $$.defaults(defaultCss, options.css);
    }
    else if (options.css) { // TODO: this is a quick fix, and should be improved (because if the user passes some options, they will be ignored)
      delete options.css.height;
      delete options.css.width;
      delete options.css.fontSize;
      delete options.css.padding;
    };

    // put button before or after?
    options.prepend = options.positionizeFirst;

    // create button and set all sort of functionalities to it
    var button = uify.button(options);

    //
    //                              ADD A FEW MORE FUNCTIONNALITIES TO BUTTON OBJECT

    // remove button in dom in and in toolbar.list
    button.remove = function () {
      button.$button.remove();
      toolbar.list.splice(button.index, 1);
    };

    //
    //                              ADD TO toolbar.list AND RETURN

    if (!toolbar.list) toolbar.list = [];
    toolbar.list.push(button);
    button.index = toolbar.list.length-1;

    return button;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});
