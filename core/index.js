var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var getAllOptions = require("./options");
var attachMethods = require("./methods");

/**
  see wiki/api.md for API
*/
var toolbarify = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  main: function (options) {

    //
    //                              GET FULL OPTIONS

    var toolbar = getAllOptions(options);
    toolbar.$el = $(toolbar.$el);
    if (toolbar.addElementClass) toolbar.$el.addClass("toolbarified");
    toolbar.id = $$.uuid();

    //
    //                              ADD ALL METHODS

    attachMethods.call(toolbarify, toolbar);


    //
    //                              MAKE TOOLBAR

    toolbar.make();
    toolbar.add.buttons(toolbar.buttons);

    //
    //                              SETUP ON RESIZE EVENT

    $$.dom.onWindowResize(function () {

      // run refresh layout method if there is one
      if (toolbar.layout && toolbar.layout.refresh) toolbar.layout.refresh();

      // check if style of toolbar should be changed
      var newStyle = toolbar.getEffectiveStyle();
      if (toolbar.effectiveStyle != newStyle) {
        toolbar.destroy();
        toolbar.make();
        var listOfButtons = toolbar.list;
        delete toolbar.list;
        toolbar.add.buttons(_.pluck(listOfButtons, "options"));
      };

    });

    //
    //                              RETURN

    return toolbar;

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REST
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // list of all toolbars (subdivided by element identifier)
  toolbarsGroupedByElement: [],
  toolbarsElGetIndex: function (toolbar) {
    return _.findIndex(toolbarify.toolbarsGroupedByElement, function (obj) {
      return toolbar.$el.is(obj.$el);
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = toolbarify;
