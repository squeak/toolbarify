# toolbarify

Make it simple to display a complex toolbar.

For the full documentation, installation instructions... check [toolbarify documentation page](https://squeak.eauchat.org/libs/toolbarify/).
