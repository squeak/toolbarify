# Usage

Load and use toolbarify library with `toolbarify(toolbarifyOptions)`.  
Or in more details:

```javascript
const toolbarify = require("toolbarify");
toolbarify({
  $el: $("body"),
  position: "right",
  buttons: [
    {
      position: "start",
      icomoon: "question",
      title: "say hello", // title (shown when you hover a few seconds)
      key: "shift + h", // keyboard shortcut
      click: function () {
        alert("Hello")
      },
    },
    {
      position: "end",
      icomoon: "linux",
      title: "I'm tux",
      key: "shift + t",
      click: function () {
        console.log("tux tux tux tux tux tux tux tux tux tux tux tux tux tux");
      },
    },
  ],
});
```

You will also need to import styles in your stylesheet, for example like this (but this may vary depending on how you setup less):

```css
@import "toolbarify/styles/index";
```

## The logic of toolbarify

Toolbarify is designed to not add itself to the element it should be linked to (the $el option), so that element is still empty when a toolbar is applied to it. However, for toolbarify to work, the element it's applied to needs to have a few basic styles applied to it: absolute positioning, css set width and height... those styles are applied automatically to the element (unless you set addElementClass to false), and they are necessary, so you should make sure your element is displayed in a way that works with toolbarify's behaviour.
The basic styles applied to the element that has a toolbar attached to it are applied through the `toolbarified` class.
