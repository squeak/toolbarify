var toolbarify = require("../../../core");
var uify = require("uify");

window.demoStart = function (utilities) {

  utilities.section({
    title: "toolbarify",
    demo: function ($container) {

      //
      //                              SETUP CONTAINER AND ELEMENT

      $container.css({
        height: 500,
        position: "relative",
      });
      var $toolbarContainer = $container.div();

      uify.button({
        $container: $toolbarContainer,
        icomoon: "plus",
        title: "add toolbar",
        inlineTitle: "bottom",
        css: { fontSize: "3em", },
        click: newToolbar,
      });

      //
      //                              CREATE TOOLBAR

      function newToolbar () {

        var toolbar = toolbarify({
          $el: $toolbarContainer,
          position: $$.random.entry(["right", "left", "top", "bottom"]),
          buttons: [
            {
              position: "start",
              icomoon: "question",
              title: "say hello",
              click: function () { alert("Hello") },
            },
            {
              position: "start",
              icomoon: "rocket",
              title: "say bye",
              click: function () { alert("bye"); },
            },
            {
              position: "start",
              icomoon: "plus",
              title: "add button in toolbar",
              key: "shift + n", // keyboard shortcut
              click: function (e) {
                e.stopPropagation(); // do not close toolbar (if condensed mode)
                toolbar.add.button({ position: "end", title: "added button", icomoon: "heartbeat", });
              },
            },
            {
              position: "end",
              icomoon: "linux",
              title: "I'm tux",
              key: "shift + t",
              click: function () {
                alert("tux tux tux tux tux tux tux tux tux tux tux tux tux tux");
              },
            },
          ],
        });

        // randomize color of toolbar
        toolbar.$toolbar.css("background-color", $$.random.color(1));

      };

      //                              ¬
      //

    },

  });

};
