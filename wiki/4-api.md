# API

🛈 This API is written using squyntax notation, information/documentation about it is available [here](https://squeak.eauchat.org/libs/squyntax/). 🛈


## Dependencies

To display properly, toolbarify uses some less styles. If you ever need to use toolbarify in a context without less, feel free to post an issue to request provision of css stylesheets.

## All options

```javascript
// options to pass to tablify
toolbarify·options = {

  !$el: <yquerjProcessable (anything that yquerj/jquery can handle)>@default="body",
  ?position: <"left"|"right"|"top"|"bottom">@default="right",
  ?style: <"condensed"|"menu"|"hidden">@default="condensed",
  ?style_smallScreens: <"condensed"|"menu"|"hidden">@default="menu",
  ?smallScreenThreshold: <function():<boolean> «
    custom function to calculate if screen is small,
    returning true means the screen is small
  »,
  ?addElementClass: <boolean>@default=true « add toolbarified class to target element ($el) so basic styles are applied to it »,
  ?buttons: <toolbarify.button·options[]> « see toolbarify.button documentation »,

  // the following options are useful only for condensed style toolbars
  ?size: <number>@default=30,
  ?resetToStylesheetStylesOnResize: <boolean> «
    pass true here if $el has some style from css stylesheet that should be reused to calculate dimensions on resize
    do not use this if you set the width and height for $el dynamically from js on resize
  »,
  ?color: <string> « use this if you want to customize the color of the toolbar »,
  ?class: <string> « a custom class to apply to the toolbar element »,

  ?displayToolbarFab: <boolean>@default=false « if true, will display toolbar fab (when in menu mode), will not just make toolbar swipable »,

  /**
    NOTE(S):
      - on small screens, toolbar will be hidden, and displayed as menu on swipe
        * at the moment, bottom and top toolbar swiping may not work
        * the container of the toolbar should have overflow hidden, at least in the direction of the toolbar
        * there is for the moment no support for multiple toolbars on the same side, since swipe areas will overlap each other (but is it really a use-case?)
      - currently if there is a left or top margin set for $el they will be replaced
  */
};
```

## The returned object

```javascript
// object returned by tablify
toolbarify·return = {
  // TODO: the list here is not exhaustive, and some values are not present, depending on the style of toolbar: improve this documentation, cleanup and simplify

  $el: <yquerjObject>,
  ... all keys and values passed in toolbarify·options

  effectiveStyle: <"menu"|"buttons"> « the style that was decided, depending on the screen size »,

  where: <"before"|"after">, // for condensed toolbar
  orientation: <"horizontal"|"vertical">, // for condensed toolbar

  add: {
    button: <function(<toolbarify·buttonOptions>):<toolbarify·buttonReturn>>,
    buttons: <function(<toolbarify·buttonOptions>[]):<void>>,
    separator: <function(<toolbarify·buttonOptions>):<toolbarify·buttonReturn>>,
    blank: <function(<toolbarify·buttonOptions>):<toolbarify·buttonReturn>>,
    progressBar: <function(<toolbarify·buttonOptions>):<toolbarify·buttonReturn>>,
    spinner: <function(<toolbarify·buttonOptions>):<toolbarify·buttonReturn>>,
  },

  list: <toolbarify·buttonReturn[]> « the list of buttons »,
  get: <function(!buttonName<string>):buton> « get the specified button by it's name property »,

  $toolbar: <yquerjObject>,

  layoutPosition: <{ top: <number>, bottom: <number>, left: <number>, right: <number>, }> « position of the toolbar »,
  elId: <number> « id or this toolbar element in toolbarify.toolbarsGroupedByElement »,
  toolbars: <toolbar[]> « list of all toolbars attached to the same element »,

  toggle: <function(noTransition <boolean> « if true, disables transition on menu toolbar »)> « toggle toolbar display »,
  show:   <function(noTransition <boolean> « if true, disables transition on menu toolbar »)> « show toolbar »,
  hide:   <function(noTransition <boolean> « if true, disables transition on menu toolbar »)> « hide toolbar »,

}

// possible button options
toolbarify·buttonOptions = {
  ?name: <string> « a name for this button »,
  !position: <"start"|"end"|"middle"> « the position of the button in the toolbar »,
  ?positionizeFirst: <boolean> « if true, will put button before others with same position »,
  ?type: <"separator"|"blank"|"progressBar"|"spinner"> « if passed will bypass any additional button options and create a button of the given type »,
  ... any other uify.button·options
}

// returned button
toolbarify·buttonReturn = {
  name: <string> « button name (if passed in options) »,
  index: <number> « index of this button in toolbar.list »,
  remove: <function(ø)> « remove this button from toolbar »,
  ... all key values from uify.button·return
}
```

## A little note

This library has been designed to be used in [electrode](https://framagit.org/squeak/electrode). It may be used in other contexts, but setting it up could be a bit more complex.
