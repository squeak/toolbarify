
Create toolbars in your page.
The toolbars are responsive and adapt to screen size. In a small screen / touch screen, a button will be displayed instead of a full toolbar. This button opens a menu containing the list of toolbar buttons.
